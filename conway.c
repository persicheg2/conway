#include "time.h"
#include "stdlib.h"
#include "string.h"
#include "unistd.h"
#include "stdio.h"

#define COL 100
#define ROW 50

int front[COL][ROW]={0};
int back[COL][ROW]={0};

void display() {
    for (int y = 0; y < ROW; y++) {
        for (int x = 0; x < COL; x++) {
            if (!front[x][y])
                printf(" ");
            else printf("█");
        }
        printf("\n");
    }
}

int mod(int a, int b) {
    return (a % b + b) % b;
}

int calculate_neighbors(int cx, int cy) {
    int neighbors = 0;
    for (int dy = -1; dy <= 1; dy++)
        for (int dx = -1; dx <= 1; dx++) {
            if (!(dx == 0 && dy == 0))
                if (front[mod(cx+dx, COL)][mod(cy+dy, ROW)])
                    neighbors += 1;
        }
    return neighbors;
}

void next_gen() {
    for (int y = 0; y < ROW; y++) {
        for (int x = 0; x < COL; x++) {
            int neighbors = calculate_neighbors(x, y);
            if (front[x][y]){
                back[x][y] = neighbors == 3 || neighbors == 2;
            } else {
                back[x][y] = neighbors == 3;
            }
        }
    }
}

int main() {
    srand(time(NULL));
    for (int y = 0; y < ROW; y++)
        for (int x = 0; x < COL; x++)
            front[x][y] = rand()%2;
    /* front[1][0] = 1; */
    /* front[2][1] = 1; */
    /* front[0][2] = 1; */
    /* front[1][2] = 1; */
    /* front[2][2] = 1; */

    for(;;){
        display();
        next_gen();
        memcpy(front, back, sizeof(front));
        printf("\033[%dA\033[%dD", ROW, COL);
        usleep(100000);
    }

    return 0;
}
